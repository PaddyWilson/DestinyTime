$(document).ready(function(){
	
	load();

	//form button click
	$( "#searchForPlayer" ).click(function() {
		search();
	});
	//form enter (submit)
	$("#searchForm").submit(function(e) {
	    search();
	    e.preventDefault(); // avoid to execute the actual submit of the form.
	});

	var debugJson = false;
	
	if(debugJson){
		$('#debug').html("<pre id=\"output\"></pre>");
	}

});

function debugJson(data){
	if(debug){
		//show json data
		var formattedData = JSON.stringify(data, null, '\t');
		$('#output').text(formattedData);
	}
}

function bungienetErrors(data)
{
	if(data.ErrorStatus != "Success"){
		console.log("Bungie.net Error: " + data.Message);
		loadingDisplay("Bungie.net Error: " + data.Message);
		$("#info").html("");
		$("#searchPlatform").html("");
		return false;
	}
	return true;
}

$(document).on("click", ".playerButton", function(){
	
    var id = $(this).data('id');
	getProfile(id.split(' ')[1], id.split(' ')[0]);
});

function search()
{
	//$( "#searchForPlayer" ).prop( "disabled", true );
	var player = $("#searchForPlayerInput").val();

	//reset some elements and data
	$('#search').html("");
	dates={}
	gameCount = 0;
	lastPlayed = '';

    loadingDisplay("Loading. Searching for player.");
    
    //http://www.bungie.net/d1/Platform/Destiny/SearchDestinyPlayer/{membershipType}/{displayName}/
    
    ajaxGet("https://www.bungie.net/d1/platform/Destiny/SearchDestinyPlayer/-1/"+player+"/", function(data){
		debugJson(data);
		if(!bungienetErrors(data)){
			return;
		}
	
		if(data.Response.length > 1){
			var searchOutput ="";
			for (var index = 0; index < data.Response.length; index++) {
				var element = data.Response[index];
				searchOutput+='<button class="btn btn-default playerButton" data-id="'+element.membershipId+' '+element.membershipType+'">'+
					'<img src="https://www.bungie.net/'+element.iconPath+'">'
					+element.displayName+'</button>';
            }
            loadingDisplay("Pick a profile");
			$("#search").html(searchOutput);
		}
		else if (data.Response.length == 1){
			getProfile(data.Response[0].membershipType,data.Response[0].membershipId);
		}
		else{
			$("#search").html("Can't find user " + player);
		}
	});
}

var dates={};
var gameCount = 0;
var lastPlayed;

//how to tell when loading is done
var loadingCount = 0;
var atLoadingCount = 0;

function getProfile(type, id)
{
    dates={}
    gameCount = 0;
    lastPlayed = '';
    
    //https://www.bungie.net/d1/Platform/Destiny/{membershipType}/Account/{destinyMembershipId}/Summary/
	ajaxGet("https://www.bungie.net/d1/Platform/Destiny/"+type+"/Account/"+id+"/Summary/", function(data){
		debugJson(data);
		if(!bungienetErrors(data)){
			return;
		}

		loadingDisplay("Loading. Character info.");

		if(data.ErrorStatus != "Success"){
			$("#info").html("No Data");
			return;
		}

        loadingCount = data.Response.data.characters.length;
        atLoadingCount = 0;

        var datesTemp=[];
        for (var index = 0; index < data.Response.data.characters.length; index++){
            datesTemp.push(new Date(data.Response.data.characters[index].characterBase.dateLastPlayed));
        }
        datesTemp = datesTemp.sort(function sortDates(a, b){return a.getTime() - b.getTime();});

		lastPlayed = datesTemp[datesTemp.length - 1];
        //lastPlayed = new Date(lastPlayed.getFullYear(), lastPlayed.getMonth(), lastPlayed.getDate());

		for (var index = 0; index < data.Response.data.characters.length; index++){
		 	getActivityHistory(type, id, data.Response.data.characters[index].characterBase.characterId,0);
		}

        displayInfo();

		//show json data
		//  var formattedData = JSON.stringify(data, null, '\t');
		//  $('#output').text(formattedData);
	});
}

function getActivityHistory(type, id, characterId, page)
{
    //http://www.bungie.net/d1/Platform/Destiny/Stats/ActivityHistory/{membershipType}/{destinyMembershipId}/{characterId}/
	ajaxGet("https://www.bungie.net/d1/Platform/Destiny/Stats/ActivityHistory/"+type+"/"+id+"/"+characterId+"/?mode=none&count=250&page="+page, function(data){
		debugJson(data);
		if(!bungienetErrors(data)){
			return;
		}
		loadingDisplay("Loading. Activity history.");

		pageTemp = page +1;
	
		if(data.Response.data.activities != null){
			gameCount = gameCount + data.Response.data.activities.length;
		
			for(var i = 0; i < data.Response.data.activities.length; i++){
				var element = data.Response.data.activities[i];
				
                var dt = new Date(element.period);
                dt = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate()).getUnixTime();
				if(dates[dt] >= 1){
					dates[dt] +=1
				}
				else{
					dates[dt] = 1;				
				}
			}

			if(data.Response.data.activities.length == 250){
				getActivityHistory(type, id, characterId, pageTemp);
            }
            else{
                atLoadingCount+=1;
            }
		}//end if(!null)

		//checks if loading is done
        if(atLoadingCount == loadingCount){
            loadingDisplay("Done Loading.");
        }

		displayInfo();	
		try{
			updateCalanders();
		}
		catch(err){/* i dont care what happens */}

		//show json data
		// var formattedData = JSON.stringify(data, null, '\t');
		// $('#output').text(formattedData);
	});
}

function displayInfo()
{
	var info = '';

	info += 'Last time played : ' + lastPlayed;
	info += '<br> Number of games : ' + gameCount;

	$("#info").html(info);

}

function loadingDisplay(info)
{
	$("#loading").html(info);
}

//for getting the unix time
Date.prototype.getUnixTime = function() { return this.getTime()/1000|0 };
if(!Date.now) Date.now = function() { return new Date(); }
Date.time = function() { return Date.now().getUnixTime(); }
//for getting the unix time

//months are 0 based
var today = new Date();
today.setDate(today.getDate() + 1);
var start = new Date(2014, 8, 1)
var months = ((start.getYear() - today.getYear()) * 12 + (start.getMonth() - today.getMonth())) * -1 + 1;
var release = new Date(2014, 8, 9);
var darkBelow = new Date(2014, 11, 9);
var houseWolves = new Date(2015, 4, 19);
var TTK = new Date(2015, 8, 15);
var RiseOfIron = new Date(2016, 8, 20);

var d2Release = new Date(2017, 8, 6);

var calArray = [];
var numberOfCalanders = 0;
function load(data){
	numberOfCalanders = (today.getYear() - start.getYear() + 1);

	var c = ""
	for (var i = 0; i < numberOfCalanders; i++) {
		c = c+'<div id="cal-'+i+'"></div>'
	}

	$("#calander").html(c);

	for (var i = 0; i < numberOfCalanders; i++) {
		calArray.push(new CalHeatMap());
	}
	
	var j = 0;
	for (var i = calArray.length - 1; i >= 0; i--) {
		calArray[i].init({
			start: new Date().setFullYear(2014 + j),
			itemSelector: "#cal-"+j,
			range: 1,
			rowLimit: 7,
			domain: "year",
			subDomainTextFormat: "%d",
			subDomain: "day",
			data: data,
			cellSize: 19.5,
			displayLegend: false,
			tooltip: true,
			cellPadding: 0,
			weekStartOnMonday: false,

			label: {
				position: "top"
			},
			highlight : [release, darkBelow, houseWolves, TTK,RiseOfIron, d2Release]
			
		});
		j = j+1;
	}
}

function updateCalanders(){
    for (var i = calArray.length - 1; i >= 0; i--){
        calArray[i].update(dates);
    }
}

function ajaxGet(url, done) {
    $.ajax({
		url: url,
		dataType: "json",
		type: "GET",		
		beforeSend: function(request) {
			request.setRequestHeader("X-API-Key", '27ccaa311ddb4367be175db39c1d1adf');// gitlab api key
		},
    })
    .done(done)
    .fail(function(jqXhr, textStatus, errorThrown) {
   		$("#loading").html("Failed");
      	$("#searchForPlayer").prop("disabled", false);
      	console.log("ajax Error " + textStatus);
    });
}