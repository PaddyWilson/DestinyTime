$(document).ready(function(){
	
	load();

	//form button click
	$( "#searchForPlayer" ).click(function() {
		searchForBNetUser();
	});
	//form enter key hit (submit)
	$("#searchForm").submit(function(e) {
	    searchForBNetUser();
	    e.preventDefault(); // avoid to execute the actual submit of the form.
	});

	var debugJson = false;

	if(debugJson){
		$('#debug').html("<pre id=\"output\"></pre>");
	}

});

function debugJson(data){
	if(debug){
		//show json data
		var formattedData = JSON.stringify(data, null, '\t');
		$('#output').text(formattedData);
	}
}

$(document).on("click", ".bNetUserButton", function(){	
	var id = $(this).data('id');
	seachDestinyID(id);
});

$(document).on("click", ".platformButton", function(){	
	var id = $(this).data('id');
	getProfile(id.split(' ')[1], id.split(' ')[0]);
});

//var debug = fase;

//step 1
function searchForBNetUser()
{
	//$( "#searchForPlayer" ).prop( "disabled", true );
	var player = $("#searchForPlayerInput").val();

	//reset some elements and data
	$('#searchBnet').html("");
	$('#searchPlatform').html("");
	dates={}
	gameCount = 0;
	lastPlayed = '';
	updateCalanders();

	loadingDisplay("Loading. Searching for BNet User.");

	console.log("searchForBNetUser()");

	//get the membership id for the user
	ajaxGet("https://www.bungie.net/platform/User/Search/Prefix/"+player+"/0/", function(data){
		debugJson(data);

		if(!bungienetErrors(data)){
			return;
		}

		if(data.Response.searchResults.length > 1){
			console.log("searchForBNetUser() > 1");
			var searchOutput ="Select BNet User ";

			var searchResults = data.Response.searchResults;

			//create buttons for different users
			for (var searchReultsindex = 0; searchReultsindex < searchResults.length; searchReultsindex++) {				

				var image = '';
				var crossSaveOverride = searchResults[searchReultsindex].destinyMemberships[0].crossSaveOverride;
				var membershipId = 0;
				var displayName = "NULL";

				//crosssave enabled
				if(crossSaveOverride > 0)
				{
					for (var index = 0; index < searchResults[searchReultsindex].destinyMemberships.length; index++)
					{
						var element = searchResults[searchReultsindex].destinyMemberships[index];
						if(element.membershipType == crossSaveOverride)
						{						
							membershipId = element.membershipId;
							image = element.iconPath;
							displayName = element.displayName;
						}
					}

					//not sure if custum profile pics work any more?
					//this might not be needed
					//if(element.profilePicturePath.includes('http')){
					//	image = element.profilePicturePath;
					//}
					//else{
					//	image = 'https://www.bungie.net/'+image;
					//}
					//button output
					searchOutput+='<button class="btn btn-default bNetUserButton" data-id="'+element.membershipId+'" style"filter: brightness(50%);">'+
						'<img src="https://www.bungie.net/'+image+'" height="32" width="32"> '+displayName+'</button>';

				}
				//crosssave disabled
				else 
				{					
					for (var index = 0; index < searchResults[searchReultsindex].destinyMemberships.length; index++)
					{
						var element = searchResults[searchReultsindex].destinyMemberships[index];
						membershipId = element.membershipId;
						membershipId = element.membershipId;
						image = element.iconPath;
						displayName = element.displayName;

						searchOutput+='<button class="btn btn-default bNetUserButton" data-id="'+element.membershipId+'" style"filter: brightness(50%);">'+
						'<img src="https://www.bungie.net/'+image+'" height="32" width="32"> '+displayName+'</button>';
					}
				}
				

				// if(element.profilePicturePath.includes('http')){
				// 	image = element.profilePicturePath;
				// }
				// else{
				// 	image = 'https://www.bungie.net/'+element.profilePicturePath;
				// }
				// //button output
				// searchOutput+='<button class="btn btn-default bNetUserButton" data-id="'+element.membershipId+'">'+
				// 	'<img src="'+image+'" height="32" width="32"> '
				// 	+element.displayName+'</button>';
			}
			$("#searchBnet").html(searchOutput);
			loadingDisplay("Done. Search for BNet User.");
		}
		//one user found
		else if (data.Response.searchResults.length === 1){
			
			var searchResults = data.Response.searchResults;
			//check if user has crosssave enable
			var crossSaveOverride = searchResults[0].destinyMemberships[0].crossSaveOverride;
			var membershipId = 0;			

			if (crossSaveOverride > 0)
			{
				console.log("cross 0");
				for (var index = 0; index < searchResults[0].destinyMemberships.length; index++)
				{
					console.log("cross 1 " + index);
					var element = searchResults[0].destinyMemberships[index];
					if(element.membershipType == crossSaveOverride)
					{						
						console.log("cross 2");
						membershipId = element.membershipId;
					}
				}
			}


			console.log("searchForBNetUser() Only 1 MembershipID=" + membershipId + " Type=" + crossSaveOverride);
			seachDestinyID(membershipId);
		}
		//no users found
		else{
			console.log("searchForBNetUser() No Bnet users with this name");
			$("#searchBnet").html("Can't find BNet user " + player);
			searchDestinyUsers();
		}
		
		

		//$( "#searchForPlayer" ).prop( "disabled", false );
	});
}

//backup step 1
//searches destiny for user not bnet users
function searchDestinyUsers()
{
	//$( "#searchForPlayer" ).prop( "disabled", true );
	var player = $("#searchForPlayerInput").val();

	dates={}
	updateCalanders();

	loadingDisplay("Loading. Searching for player. Destiny");
	console.log("searchDestinyUsers()");

	ajaxGet("https://www.bungie.net/platform/Destiny2/SearchDestinyPlayer/-1/"+player+"/", function(data){
		debugJson(data);
		
		if(!bungienetErrors(data)){
			return;
		}

		if(data.Response.length > 1){
			console.log("searchDestinyUsers() >1");
			var searchOutput ="";
			for (var index = 0; index < data.Response.length; index++) {
				var element = data.Response[index];
				searchOutput+='<button class="btn btn-default platformButton" data-id="'+element.membershipId+' '+element.membershipType+'">'+
					'<img src="https://www.bungie.net/'+element.iconPath+'">'
					+element.displayName+'</button>';
			}
			$("#search").html(searchOutput);
		}
		else if (data.Response.length == 1){
			console.log("searchDestinyUsers() 1");
			getProfile(data.Response[0].membershipType,data.Response[0].membershipId);
		}
		else{
			console.log("searchDestinyUsers() 0");
			$("#search").html("Can't find user " + player);
		}

		//$( "#searchForPlayer" ).prop( "disabled", false );
	});
}

//step 2
function seachDestinyID(membershipID){
	console.log("seachDestinyID()");

	dates={}
	updateCalanders();

	loadingDisplay("Loading player data.");

	$('#searchPlatform').html("");

	ajaxGet("https://www.bungie.net/platform/User/GetMembershipsById/"+membershipID+"/-1/", function(data){
		debugJson(data);	
		
		if(!bungienetErrors(data)){
			return;
		}		

		var destinyMemberships = data.Response.destinyMemberships;

		var crossSaveOverride = data.Response.destinyMemberships[0].crossSaveOverride;

		//no profiles found
		if(destinyMemberships.length == 0)
		{
			console.log("seachDestinyID() no profiles found");
			$("#searchPlatform").html("Can't find information for the bnet profile");
			searchDestinyUsers();
		}
		else if(destinyMemberships.length == 1)
		{
			console.log("seachDestinyID() one profile found");
			getProfile(destinyMemberships[0].membershipType, destinyMemberships[0].membershipId);
		}
		//cross save enabled
		else if(crossSaveOverride > 0)
		{
			console.log("seachDestinyID() crosssave enabled");
			destinyMemberships.forEach(element => {
				if(element.membershipType == crossSaveOverride)
				{										
					getProfile(element.membershipType, element.membershipId);
				}
			});
		}
		//crossSave disabled
		else
		{
			console.log("seachDestinyID() crosssave disabled");

			var searchOutput ="Select platform ";

			destinyMemberships.forEach(element => {
				searchOutput+='<button class="btn btn-default platformButton" data-id="'+element.membershipId+' '+element.membershipType+'">'+
					'<img src="https://www.bungie.net/'+ element.iconPath +'" height="32" width="32">'
					+element.displayName+'</button>';
			});

			$("#searchPlatform").html(searchOutput);
		}

		loadingDisplay("Done.");

	});//end ajax
}


var dates={};
var gameCount = 0;
var lastPlayed;

//how to tell when loading is done
var loadingCount = 0;
var atLoadingCount = 0;

//step 3
function getProfile(type, id)
{
	dates={}
	gameCount = 0;
	lastPlayed = '';

	console.log("getProfile() " + type + ' ' + id);

	ajaxGet("https://www.bungie.net/Platform/Destiny2/"+type+"/Profile/"+id+"/?components=100", function(data){
		debugJson(data);
		
		if(!bungienetErrors(data)){
			return;
		}
		
		//$("#searchPlatform").html("");
		loadingDisplay("Loading. Character info.");

		if(data.ErrorStatus != "Success"){
			console.log("getProfile() ErrorStatus " + data.ErrorStatus);

			$("#info").html("No Data");
			loadingDisplay("Done");
			return;
		}
		
        loadingCount = data.Response.profile.data.characterIds.length;
		atLoadingCount = 0;

		lastPlayed = new Date(data.Response.profile.data.dateLastPlayed);

		for (var index = 0; index < data.Response.profile.data.characterIds.length; index++){
			getActivityHistory(type, id, data.Response.profile.data.characterIds[index],0);
		}
	});
}

function getActivityHistory(type, id, characterId, page)
{
	ajaxGet("https://www.bungie.net/Platform/Destiny2/"+type+"/Account/"+id+"/Character/"+characterId+"/Stats/Activities/?count=250&page="+page, function(data){
		debugJson(data);
		
		if(!bungienetErrors(data)){
			return;
		}

		loadingDisplay("Loading. Activity history.");

		pageTemp = page +1;
	
		if(data.Response.activities != null){
			gameCount = gameCount + data.Response.activities.length;
		
			for(var i = 0; i < data.Response.activities.length; i++){
				var element = data.Response.activities[i];
				
				var dt = new Date(element.period);
                dt = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate()).getUnixTime();
				if(dates[dt] >= 1){
					dates[dt] +=1
				}
				else{
					dates[dt] = 1;				
				}
			}

			if(data.Response.activities.length == 250){
				getActivityHistory(type, id, characterId, pageTemp);
			}else{
                atLoadingCount+=1;
            }
		}//end if(!null)
		else{
			atLoadingCount+=1;
		}

		//checks if loading is done
        if(atLoadingCount == loadingCount){
            loadingDisplay("Done Loading.");
		}
		
		displayInfo();	
		try{
			updateCalanders();
		}
		catch(err){/* i dont care what happens */}
	});
}

function bungienetErrors(data)
{
	if(data.ErrorStatus != "Success"){
		console.log("Bungie.net Error: " + data.Message);
		loadingDisplay("Bungie.net Error: " + data.Message);
		$("#info").html("");
		$("#searchPlatform").html("");
		return false;
	}
	return true;
}

function displayInfo()
{
	var info = '';

	info += 'Last time played : ' + lastPlayed;
	info += '<br> Number of games : ' + gameCount;

	$("#info").html(info);

}

function loadingDisplay(info)
{
	$("#loading").html(info);
}

//for getting the unix time
Date.prototype.getUnixTime = function() { return this.getTime()/1000|0 };
if(!Date.now) Date.now = function() { return new Date(); }
Date.time = function() { return Date.now().getUnixTime(); }
//for getting the unix time

//months are 0 based
var today = new Date();
today.setDate(today.getDate() + 1);
var start = new Date(2017, 8, 1)

var months = ((start.getYear() - today.getYear()) * 12 + (start.getMonth() - today.getMonth())) * -1 + 1;

var release = new Date(2017, 8, 6);
var pcRelease = new Date(2017, 9, 24);
var curseOfOsiris = new Date(2017, 11, 5);
var warmind = new Date(2018, 4, 8);
var forsaken = new Date(2018, 8, 4);

var calArray = [];
var numberOfCalanders = 0;
function load(data){

	numberOfCalanders = (today.getYear() - start.getYear() + 1);

	var c = ""
	for (var i = 0; i < numberOfCalanders; i++) {
		c = c+'<div id="cal-'+i+'"></div>'
	}

	$("#calander").html(c);

	for (var i = 0; i < numberOfCalanders; i++) {
		calArray.push(new CalHeatMap());
	}
	
	var j = 0;
	for (var i = calArray.length - 1; i >= 0; i--) {
		calArray[i].init({
			start: new Date().setFullYear(2017 + j),
			itemSelector: "#cal-"+j,
			range: 1,
			rowLimit: 7,
			domain: "year",
			subDomainTextFormat: "%d",
			subDomain: "day",
			data: data,
			cellSize: 19.5,
			displayLegend: false,
			tooltip: true,
			//domainDynamicDimension: false,

			cellPadding: 0,
			weekStartOnMonday: false,

			label: {
				position: "top"
			},
			highlight : [release, pcRelease, curseOfOsiris, warmind, forsaken]
			
		});
		j = j+1;
	}
}

function updateCalanders(){
    for (var i = calArray.length - 1; i >= 0; i--){
        calArray[i].update(dates);
    }
}

function ajaxGet(url, done) {
    $.ajax({
		url: url,
		dataType: "json",
		type: "GET",		
		beforeSend: function(request) {
			request.setRequestHeader("X-API-Key", '27ccaa311ddb4367be175db39c1d1adf');// gitlab api key
			
		},
    })
    .done(done)
    .fail(function(jqXhr, textStatus, errorThrown) {
   		$("#loading").html("Failed");
      	$("#searchForPlayer").prop("disabled", false);
      	console.log("ajax Error " + textStatus);
    });
}